#!/bin/dash
# 
# needs /usr/bin/time to be present
####################################

# $1 is the shell that'll execute the Xsession.d script
# $2 is the number of passes in the test
do_test_run () {
  # initialization
  listing_count=${listing_count:-`ls -1 /etc/desktop-profiles/*.listing | wc -l`};
  profile_count=${profile_count:-`list-desktop-profiles | wc -l`};
  total_real=0;
  total_user=0;
  total_system=0;
  echo "		Real	User	System	/Profile	/.listing" >> $1_$RESULT_SUFFIX;

  # perform test
  for c in `seq 1 $2`; do
    performance=`/usr/bin/time --format "%e %U %S" $1 /etc/X11/Xsession.d/20desktop-profiles_activateDesktopProfiles 2>&1`; 
    pass_real=`echo $performance | cut -d " " -f 1`;
    pass_user=`echo $performance | cut -d " " -f 2`;
    pass_system=`echo $performance | cut -d " " -f 3`;
    real_per_profile=$( (echo scale=4; echo ${pass_real} / ${profile_count}) | bc );
    real_per_listing=$( (echo scale=4; echo ${pass_real} / ${listing_count}) | bc );

    echo -n "   Pass $c: 	$pass_real	$pass_user	$pass_system" >> $1_$RESULT_SUFFIX;
    echo "	$real_per_profile	$real_per_listing" >> $1_$RESULT_SUFFIX;
    total_real=`echo ${total_real} + ${pass_real} | bc`;
    total_user=`echo ${total_user} + ${pass_user} | bc`;
    total_system=`echo ${total_system} + ${pass_system} | bc`;
  done  
  
  # output average
  real_per_profile=$( (echo scale=3; echo ${total_real} / ${profile_count}) | bc)
  echo -n "Average/$2:" >> $1_$RESULT_SUFFIX;
  echo -n "	$( (echo "scale=3" ; echo "${total_real} / $2") | bc | sed 's/^\./0./')" >> $1_$RESULT_SUFFIX;
  echo -n "	$( (echo "scale=3" ; echo "${total_user} / $2") | bc | sed 's/^\./0./')" >> $1_$RESULT_SUFFIX;
  echo -n "	$( (echo "scale=3" ; echo "${total_system} / $2") | bc | sed 's/^\./0./')" >> $1_$RESULT_SUFFIX;
  echo -n "	$( (echo scale=4; echo ${total_real} / ${profile_count} / $2) | bc)" >> $1_$RESULT_SUFFIX;
  echo -n "	$( (echo scale=4; echo ${total_real} / ${listing_count} / $2) | bc)" >> $1_$RESULT_SUFFIX;
  echo "	($profile_count profiles in $listing_count metadata files)" >> $1_$RESULT_SUFFIX;
  echo >> $1_$RESULT_SUFFIX;
}

# $1 is the directory containing the metadatafiles making up the test set
do_test_with () {
  # add note telling what where testing to resultfile
  for SHELL in $SHELL_LIST; do
    echo "Results for testset $1 with $SHELL: " >> ${SHELL}_$RESULT_SUFFIX;
  done;  

  for LISTING  in `ls $1`; do
    # add next listing, and recalculate #(profiles) and #(metadata files)
    cp $1/$LISTING /etc/desktop-profiles;
    listing_count=`ls -1 /etc/desktop-profiles/*.listing | wc -l`;
    profile_count=`list-desktop-profiles | wc -l`;
    
    for SHELL in $SHELL_LIST; do
      do_test_run $SHELL $PASS_LENGHT;
    done;  
  done;  

  for LISTING  in `ls $1`; do
    rm /etc/desktop-profiles/$LISTING
  done;  
}

#####################
# Start of execution
#####################
PASS_LENGHT=10;
TEST_SETS="10_per_file 1_per_file";
RESULT_SUFFIX="$(uname -n).results";
if (which dash > /dev/null); then
  SHELL_LIST="dash bash";
else  
  echo "The dash shell isn't present -> skipping test with dash";
  SHELL_LIST="bash";
fi;

# sanity checks:
# - check if time binary is present, shell builtin is not sufficient
# - check if we can write to /etc/desktop-profiles so we can add in 
#   our test metadata
if !(test -e /usr/bin/time); then
  echo "You need to install the time package for this script to run";
  exit 1;
fi;  

if !(test -w /etc/desktop-profiles); then
  echo "Script can't write to /etc/desktop-profiles and is thus unable to add test data.";
  echo "-> Exiting, please run script as user with write priviliges to that directory.";
  exit 1;
fi;

# do baseline test
for SHELL in $SHELL_LIST; do
  # initialize resultfile with info about the machine we're running on
  cat /proc/cpuinfo | grep "Hz\|model name" > ${SHELL}_$RESULT_SUFFIX;
  echo >> ${SHELL}_$RESULT_SUFFIX;

  # test base performance
  echo "Results for baseline test with $SHELL: " >> ${SHELL}_$RESULT_SUFFIX;
  do_test_run $SHELL $PASS_LENGHT;
done;

# do a testrun with each of the testsets
for TEST_SET in $TEST_SETS; do
  do_test_with $TEST_SET;
done;

# create summary
for SHELL in $SHELL_LIST; do
  cat ${SHELL}_$RESULT_SUFFIX | grep "Average\|Results" >> $RESULT_SUFFIX.summary;
  echo >> $RESULT_SUFFIX.summary;
done;
