.TH DESKTOP-PROFILES 7 "May 02, 2005" "desktop-profiles"

.SH NAME
desktop-profiles \- introduction and overview

.SH DESCRIPTION
Desktop-profiles offers a standard way of managing the conditional activation of installed profiles (sets of configuration and/or data files) for the various Desktop Environments in Debian.
.PP
It currently supports Freedesktop, KDE, Gconf (i.e Gnome), Xfce (>= 4.2), ROX, GNUSTEP and UDE.

.SH HOW IT WORKS
Each available profile has some metadata associated with it. On X startup an Xsession.d script is run that looks through the metadata for all profiles and activates profiles based on what it finds. 
.PP
Specifically each profile is associated with a set of requirements, and a precedence value. On X startup the Xsession.d script will filter out those profiles whose requirements are not met, and then activate the remaining profiles in order of precedence.
.PP
Exactly how a profile is activated depends on the profile kind (you don't need to know this in order to use this package):
.IP \(bu 3
For KDE, Freedesktop, Xfce (>= 4.2), ROX, GNUSTEP and UDE activating profiles is done by setting environment variables: KDEDIRS for KDE, XDG_CONFIG_DIRS and XDG_DATA_DIRS for both Freedesktop and Xfce, CHOICESPATH for ROX, GNUSTEP_PATHLIST for GNUSTEP (usually initialized from the various GNUSTEP_*_ROOT variables) and UDEdir for UDE. With the exception of UDEdir, which takes a single directory, each of these variables takes a precedence ordered list of root-directories (of activated profiles).
.IP \(bu 3
For GConf profiles two user-specific path files are generated. One containing the activated mandatory "configuration sources", one containing the default "configuration sources" that are activated. 
.IP NOTE:
Environment variables will only be set if their value is different from the default value, and user-specific path files are only generated if the systemwide gconf path file will include them. This to avoid unnecessary clutter.
.IP NOTE:
The above means that for Freedesktop, KDE, GNOME, Xfce (>= 4.2), GNUSTEP and ROX any number of profiles can be activated at the same time. Whereas UDE can only activate 1 profiles at the time.
.IP NOTE:
By default the Xsession.d script will assume the metadata files are authoritative, meaning it will ignore any values already assigned to the relevant environment variables.
.IP NOTE:
The default system-wide path contains a number of configuration sources not managed by desktop-profiles. In order to facilitate the management of all your configuration sources through desktop-profiles this package provides a script (/usr/sbin/path2listing) that looks at your current gconf configuration and adapts it so your configuration sources are all controlled by desktop-profiles (see the man page for path2listing or the /usr/share/doc/destkop-profiles/README for more information on this).

.SH INTERACTION WITH OTHER AGENTS ACTIVATING PROFILES
Since profiles are activated through environment variables one issue is how desktop-profiles deals with the situation where those environment variables have already been set up by other agents (such an agent could be another script, or the user). Desktop-profiles has a personality setting that determines how it handles such a situation:
.IP \(bu 3
autocrat: assume desktop-profiles is the only agent allowed to touch the environment variables, and consequently ignore the contents of already set environment variables.
.IP \(bu 3
rude: profiles added by desktop-profiles take precedence over profiles added by other agents.
.IP \(bu 3
polite: profiles added by other agents take precedence over profiles added by desktop-profiles.
.IP \(bu 3
sheep: just meekly follow along with what the other agents have set, don't change anything (this essentially deactivates desktop-profiles).
.PP
The default personality setting of desktop-profiles is polite.

.SH WHERE TO FIND THE PROFILE METADATA
The metadata is specified in .listing files that are placed in the /etc/desktop-profiles directory. The format of those files is specified in the 'FORMAT OF .listing FILES'-section below.
.IP NOTE:
In order to ensure that packages containing .listing files don't run in to each other, packages should install such files as <packagename>.listing, or <packagename>_<something>.listing (there's a debhelper script provided to help with that :)

.SH FORMAT OF .listing FILES
Each non-empty line in a .listing file is either a comment line, or line containing profile metadata.
.PP
Comment lines start with \'#\' and are purely for human consumption, like empty lines they are ignored completely by the Xsession.d script.
.PP
Lines containing profile metadata are made up of 6 fields separated by a semi-colon (\';\'). Where the meaning of the fields is as follows:
.IP \(bu 3
.B 1st field
: Name of the profile, arbitrary, must be unique within each file, and may (but probably should not) be empty.
.IP \(bu 3
.B 2nd field
: The kind of profile, must be set, must be one of: KDE, XDG_CONFIG, XDG_DATA, GCONF, ROX, GNUSTEP, or UDE.
.IP \(bu 3
.B 3th field: 
.IP 
Location of the root of the profile directory tree, may contain more then 1 directory (in which case directories should be separated with spaces). Environment variables may be used when specifying root directories (e.g. $HOME/.extra_config).
.IP
Except for Gconf profiles, which use the this field to contain exactly one directive to be included in the generated path file (directives are either \'xml:(readonly|readwrite):<profile-root>\', or \'include <some-path-file>' ).
.IP \(bu 3
.B 4th field
: A Numeric precedence value for the profile, may be empty (which is treated as lowest possible precedence).
.IP
When 2 (or more) active profiles define a setup for the same thing, the value specified by the profile with the highest precedence value is used (UDE will onlyuse values from the highest ranked profile).
.IP \(bu 3
.B 5th field
: Space separated list of conditions that need to be met to activate the profiles (if more then 1 condition is specified all conditions need to be met to activate the profile).
.IP
There are 3 different kinds of requirements: 
.IP  
1) <group>      = user needs to be a member of <group>
.IP  
2) !<group>     = user mustn't be a member of <group>
.IP
   (Note: '!' deactivates the profile completely)
.IP  
3) $(<command>) = <command> needs to exit successfully ($?=0)
.IP
   (Where <command> is an arbitrary shell command)
.IP \(bu 3
.B 6th field
: A description of what the profile is/does, may be empty.
.IP
Note that this basically boils down to a CSV-file using \';\' as separator and allowing shell-style comments.

.SH CREATING PROFILES
.IP \(bu 3
.B KDE 
(through KDEDIRS):
.IP
Each profile directory is layed out according to the KDE file system hierarchy (see http://techbase.kde.org/KDE_System_Administration#File_System)
.IP    
Config files in the different profiles are merged (in case of conflicting keys, the value of the highest precedence profile is used). For other files the highest precedence profile that contains the file supplies it.
.IP \(bu 3
.B Freedesktop 
(using XDG_CONFIG_DIRS and XDG_DATA_DIRS)
.IP
The 'Desktop base directory specification' defines the basic framework for using profiles (see http://freedesktop.org/wiki/Specifications/basedir-spec).
.IP
The actual contents of the profiles is filled in by things conforming to other freedesktop standards (e.g. the 'menu specification'). A list of freedesktop standards (that are being worked on) can be found at http://freedesktop.org/wiki/Specifications. Most of these standards are still under development and not (yet) widely supported. Eventually you can probably suspect support of at least KDE, GNOME, ROX, and Xfce.
.IP
Xfce (>=4.2) specific settings can also be found in Freedesktop profile dirs (see the next section for details).
.IP \(bu 3
.B Xfce
(using XDG_CONFIG_DIRS and XDG_DATA_DIRS)
.IP
Starting from Xfce version 4.2. Xfce will completely adopt the freedesktop 'Desktop Base Directory Specification'. Placing any Xfce-only settings in an 'xfce4' subdirectory of the freedesktop profile directories (with the exception of xfce4-session, which will use an 'xfce4-session' subdirectory). A more complete description can be found at http://foo-projects.org/~benny/xfce/file-locations.html.
.IP
If two profiles contain the same config file, the one from the profile with the highest precedence is used.
.IP
Xfce versions prior to 4.2. don't support multiple config sets.
.IP \(bu 3
.B ROX 
(through CHOICESPATH):
.IP
Each profile directory has one subdirectory for each app for which it provides settings. When a configuration value is needed the profile directories are searched in order, first profile that contains the file supplies it.
.IP NOTE:
Programs _may_ merge the files the different profiles. If the merging encounters conflicting values the one from the highest order profile is used. 
.IP   
See http://rox.sourceforge.net/choices.html for a more detailed description.
.IP \(bu 3
.B GNUSTEP 
(through GNUSTEP_PATHLIST)
.IP
Profiles in GNUSTEP parlance are called domains, and by default GNUSTEP will look in 4 domains (the location of which is indicated by the GNUSTEP_USER_ROOT, GNUSTEP_LOCAL_ROOT, GNUSTEP_NETWORK_ROOT, and GNUSTEP_SYSTEM_ROOT variables). Though it is possible to specify extra domains to use through the GNUSTEP_PATHLIST variable, it isn't often done as configuration files are currently only searched for in the user domain.
.IP
For more information on GNUSTEP domains see 
http://www.gnustep.org/resources/documentation/User/GNUstep/filesystem.html
.IP \(bu 3
.B UDE
(through UDEdir):
.IP
UDE searches for configuration files in the following directories (first find is used): 
.IP
1. $HOME/.ude/config
.IP
2. $UDEdir/config  (or in absence of $UDEdir in the install dir which is /usr/share/ude on debian)
.IP
3. If the configuration file is still not found, UWM takes the filename as it is (usually dereferencing any environment variables first)
.IP \(bu 3
.B GNOME 
(using GConf 'Configuration Sources'):
.IP
Two gconf path files are generated for each user on login: one with all the sources from activated profiles that have a higher precedence then the gconf-user profile (which is in default.listing), and one containing the sources from activated profiles with a lower precedence then the gconf-user profiles. Generated path files are put in /var/cache/desktop-profiles.
.IP
Each configuration source is structured like a simple hierarchical file system as follows:
.IP
- Directories correspond to applications that use the GConf repository, except for the ' schemas' directory which contains files describing all of the preference keys.
.IP
- Subdirectories correspond to categories of preferences.
.IP
- Files list the preference keys in the directory, and contain information about the keys. 
.IP
- Configuration Sources are searched in order for each value, first source that has the value (or is writeable in case of storing values) is used.
.IP    
-> See the GNOME administration manual for a detailed explanation

.SH FILES
/etc/desktop-profiles/desktop-profiles.listing - Details the default settings for the various environments. By default the system-wide settings provided by the packager are given no precedence, which means they will be loaded after all profiles with a precedence specified (which should be true for all profiles you create).
.PP
/etc/X11/Xsession.d/20desktop-profiles_activateDesktopProfiles - Xsesssion.d script that activates the profiles
.PP
/etc/default/desktop-profiles - File containing default settings for the scripts in this package.

.SH AUTHOR
This manual page was written by Bart Cornelis <cobaco@skolelinux.no>.

.SH SEE ALSO
list-desktop-profiles(1), update-profile-cache(1), dh_installlisting(1), path2listing(1)
